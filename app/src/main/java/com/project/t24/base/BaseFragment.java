package com.project.t24.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.project.t24.App;
import com.project.t24.AppComponent;
import com.project.t24.R;
import com.project.t24.util.fragment.FragmentBuilder;
import com.project.t24.util.fragment.Page;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by mac on 3.08.2016.
 */

public abstract class BaseFragment<V extends BaseFragmentView, P extends BaseFragmentPresenter<V>> extends MvpFragment<V, P> implements BaseFragmentView {

    public abstract int getLayoutId();

    public abstract void onFragmentStarted();

    protected ViewGroup container;

    public abstract Page getPage();

    private Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        container = (ViewGroup) inflater.inflate(getLayoutId(), container, false);
        injectDependencies(App.get(getContext()).component());
        unbinder = ButterKnife.bind(this, container);
        return container;
    }

    public abstract void injectDependencies(AppComponent appComponent);

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && getPresenter() != null && getPresenter().isViewAttached()) {
            onFragmentStarted();
        }
    }


    @Override
    public FragmentBuilder getPage(Page page, Object... obj) {
        return getBaseActivity().getPage(page, obj);
    }


    @Override
    public boolean isTablet() {
        return getBaseActivity() != null ? getBaseActivity().isTablet() : getResources().getBoolean(R.bool.is_tablet);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }


    @Override
    public void startActivity(Class T) {
        if (getBaseActivity() != null) {
            getBaseActivity().startActivity(T);
        }
    }

    @Override
    public void startActivity(Class T, Bundle bundle) {
        if (getBaseActivity() != null) {
            getBaseActivity().startActivity(T, bundle);
        }
    }

    @Override
    public void finishActivity() {
        if (getBaseActivity() != null) {
            getBaseActivity().finishActivity();
        }
    }

    @Override
    public void showPage(Page page, Object... obj) {
        if (getBaseActivity() != null) {
            FragmentBuilder fragmentBuilder = getPage(page, obj);

          /*  if (page == Page.DIALOG_CONTAINER) {
                fragmentBuilder.setUseDialogContainer(false);
            } */

            showPage(fragmentBuilder);
        }
    }

    @Override
    public void showPage(FragmentBuilder replacer) {
        if (getBaseActivity() != null) {
           /* if (getParentFragment() instanceof BaseDialogFragment && replacer.isUseDialogContainer()) {
                if (((BaseDialogFragment) getParentFragment()).getContainerId() != -1) {
                    replacer.setContainerId(((BaseDialogFragment) getParentFragment()).getContainerId());
                }

                replacer.setFragmentManager(getParentFragment().getChildFragmentManager());
            } */

            getBaseActivity().showPage(replacer);
        }
    }

    @Override
    public void lockScreen() {
        if (getActivity() != null)
            getBaseActivity().lockScreen();
    }

    @Override
    public void unlockScreen() {
        if (getActivity() != null)
            getBaseActivity().unlockScreen();
    }

    @Override
    public void goToPage(Page page) {
        if (getActivity() != null) {
            getBaseActivity().goToPage(page);
        }
    }

    @Override
    public void onBackPress() {
        if (getActivity() != null) {
            getBaseActivity().onBackPressed();
        }
    }

    @Override
    public void setToolbarTitle(String title) {
        if (getActivity() != null) {
            getBaseActivity().setToolbarTitle(title);
        }
    }

    @Override
    public void closeLeftMenu() {
        if (getBaseActivity() != null) {
            getBaseActivity().closeLeftMenu();
        }
    }

}
