package com.project.t24.base;

import android.content.Context;
import android.os.Bundle;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.project.t24.util.fragment.FragmentBuilder;
import com.project.t24.util.fragment.Page;

/**
 * Created by mac on 11.09.2016.
 */

public interface BaseFragmentView extends MvpView {
    Context getContext();

    void startActivity(Class T);

    void startActivity(Class T, Bundle bundle);

    void finishActivity();

    void lockScreen();

    void unlockScreen();

    boolean isTablet();

    void goToPage(Page page);

    void onBackPress();

    void showPage(Page page, Object... obj);

    void showPage(FragmentBuilder replacer);

    FragmentBuilder getPage(Page page, Object... obj);

    void setToolbarTitle(String title);

    void closeLeftMenu();
}
