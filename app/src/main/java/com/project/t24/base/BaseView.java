package com.project.t24.base;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.project.t24.data.response.Category;
import com.project.t24.util.fragment.FragmentBuilder;
import com.project.t24.util.fragment.Page;

import java.util.ArrayList;

/**
 * Created by mac on 3.08.2016.
 */

public interface BaseView extends MvpView {

    Context getContext();

    void startActivity(Class T);

    void startActivity(Class T, Bundle bundle);

    void finishActivity();

    void lockScreen();

    void setContentView();

    void unlockScreen();

    void setToolbarTitle(String title);

    void showLog(String className);

    boolean isTablet();

    FragmentBuilder getPage(Page page, Object... obj);

    void showPage(Page page, Object... obj);

    void showPage(FragmentBuilder replacer);

    void setLeftMenuItems(ArrayList<Category> category);

    void addLeftMenuItems(Menu menu);

}
