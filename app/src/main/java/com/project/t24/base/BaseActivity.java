package com.project.t24.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.project.t24.App;
import com.project.t24.AppComponent;
import com.project.t24.R;
import com.project.t24.data.response.Category;
import com.project.t24.ui.main.controller.MainActivity;
import com.project.t24.util.KeyboardUtils;
import com.project.t24.util.fragment.FragmentBuilder;
import com.project.t24.util.fragment.FragmentFactory;
import com.project.t24.util.fragment.FragmentTransactionType;
import com.project.t24.util.fragment.Page;
import com.project.t24.widgets.OnBackPressedListener;
import com.project.t24.widgets.loading.ProgressLoadingDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mac on 3.08.2016.
 */

public abstract class BaseActivity<V extends BaseView, P extends BasePresenter<V>> extends MvpActivity<V, P> implements BaseView, DrawerLayout.DrawerListener {


    @Nullable
    @BindView(R.id.activityBase_vsContainer)
    public FrameLayout flContainer;
    @BindView(R.id.activityBase_tbToolbar)
    @Nullable
    Toolbar tb;
    private DrawerLayout dl;
    private NavigationView nv;
    private ActionBarDrawerToggle dt;
    private Context context;
    private boolean isTablet;
    private OnBackPressedListener onBackPressedListener;
    private ArrayList<Category> category;

    public abstract void onActivityStarted();

    public abstract void injectDependencies(AppComponent appComponent);

    public boolean isUseLeftMenu() {
        return true;
    }

    public boolean isBackEnable() {
        return false;
    }

    public int getLayoutId() {
        return -1;
    }

    @Override
    public boolean isTablet() {
        return isTablet;
    }

    public String getToolbarTitle() {
        return "";
    }

    @Override
    public void setToolbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void setLeftMenuItems(ArrayList<Category> category) {
        this.category = category;
        Menu menu = nv.getMenu();
        addLeftMenuItems(menu);
    }


    @Override
    public void addLeftMenuItems(Menu menu) {
        if (category != null) {
            for (int i = 0; i < category.size(); i++) {
                menu.add(category.get(i).getName());
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        injectDependencies(App.get(this).component());
        super.onCreate(savedInstanceState);
        context = this;
        setContentView();
        ButterKnife.bind(this);
        inflateToolbarLayout();
        onActivityStarted();
    }

    @Override
    public FragmentBuilder getPage(Page page, Object... obj) {
        return FragmentFactory.getInstance().getFragment(isTablet(), page, obj);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_left, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setContentView() {
        if (getLayoutId() != -1)
            super.setContentView(getLayoutId());
        else if (isUseLeftMenu()) {
            super.setContentView(R.layout.activity_base);
            initializeDl();
        } else
            super.setContentView(R.layout.activity_base_no_left_menu);

    }


    public void initializeDl() {
        dl = (DrawerLayout) this.findViewById(R.id.activityBase_dl);
        nv = (NavigationView) this.findViewById(R.id.activityBase_nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int categoryId = findCategoryId(item);
                String categoryName = findCategoryName(item);
                getPresenter().onLeftMenuItemSelected(categoryId, categoryName);
                return true;
            }
        });
    }

    private String findCategoryName(MenuItem item) {
        for (int i = 0; i < category.size(); i++) {
            if (category.get(i).getName().equals(item.toString()))
                return category.get(i).getName();
        }
        return "";
    }

    private int findCategoryId(MenuItem item) {
        for (int i = 0; i < category.size(); i++) {
            if (category.get(i).getName().equals(item.toString()))
                return Integer.valueOf(category.get(i).getId());
        }
        return -1;
    }


    @Override
    public void showPage(Page page, Object... obj) {
        showPage(getFactory().getFragment(isTablet(), page, obj));
    }

    @Override
    public void showPage(FragmentBuilder replacer) {
        Page page = null;

        switch (replacer.getPageType()) {
            case NORMAL:
                page = replacer.getFragment().getPage();
                break;
            case DIALOG:
                // page = replacer.getDialogFragment().getPage();
                break;
        }

        String TAG;
        if (!TextUtils.isEmpty(replacer.getTag())) {
            TAG = replacer.getTag();
        } else {
            TAG = page.name();
        }


        FragmentManager fManager = replacer.getFragmentManager() != null ? replacer.getFragmentManager() : getSupportFragmentManager();

        FragmentTransaction fTransaction = fManager.beginTransaction();

        int containerId = -1;

        if (replacer.isSettedContainer())
            containerId = replacer.getContainerId();
        else if (flContainer != null)
            containerId = flContainer.getId();

        if (replacer.isDialog()) {
            //replacer.getDialogFragment().show(fManager, null);
        } else {
            if (!replacer.isSettedFragment()) {
                throw new RuntimeException(getString(R.string.fragment_empty_error));
            } else {

                switch (replacer.getTransactionAnimation()) {
                    case ENTER_FROM_LEFT:
                        fTransaction.setCustomAnimations(
                                R.anim.anim_horizontal_fragment_in_from_pop, R.anim.anim_horizontal_fragment_out_from_pop,
                                R.anim.anim_horizontal_fragment_in, R.anim.anim_horizontal_fragment_out);
                        break;
                    case ENTER_FROM_RIGHT:
                        fTransaction.setCustomAnimations(
                                R.anim.anim_horizontal_fragment_in, R.anim.anim_horizontal_fragment_out,
                                R.anim.anim_horizontal_fragment_in_from_pop, R.anim.anim_horizontal_fragment_out_from_pop);
                        break;
                    case ENTER_FROM_BOTTOM:
                        fTransaction.setCustomAnimations(
                                R.anim.anim_vertical_fragment_in, R.anim.anim_vertical_fragment_out,
                                R.anim.anim_vertical_fragment_in_from_pop, R.anim.anim_vertical_fragment_out_from_pop);
                        break;
                    case ENTER_WITH_ALPHA:
                        fTransaction.setCustomAnimations(
                                R.anim.anim_alphain, R.anim.anim_alphaout,
                                R.anim.anim_alphain, R.anim.anim_alphaout);
                        break;
                    case ENTER_FROM_RIGHT_STACK:
                        fTransaction.setCustomAnimations(
                                R.anim.anim_open_next, R.anim.anim_close_main,
                                R.anim.anim_open_main, R.anim.anim_close_next);
                        break;
                    case ENTER_FROM_RIGHT_NO_ENTRANCE:
                        fTransaction.setCustomAnimations(
                                0, R.anim.anim_horizontal_fragment_out,
                                R.anim.anim_horizontal_fragment_in_from_pop, R.anim.anim_horizontal_fragment_out_from_pop);
                        break;
                    case NO_ANIM:
                    default://no animation
                        break;
                }

                if (containerId == -1)
                    throw new RuntimeException(getString(R.string.fragment_empty_error));

                if (replacer.isClearBackStack()) {
                    goToPage(null);
                }

                if (replacer.getTransactionType() == FragmentTransactionType.REPLACE) {
                    fTransaction.replace(containerId, replacer.getFragment(), TAG);
                } else {
                    fTransaction.add(containerId, replacer.getFragment(), TAG);
                }

                if (replacer.isAddToBackStack()) {
                    fTransaction.addToBackStack(TAG);
                }

                fTransaction.commitAllowingStateLoss();
            }
        }
    }

    public void goToPage(Page page) {
        if (page != null) {
            getSupportFragmentManager().popBackStack(page.name(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public FragmentFactory getFactory() {
        return FragmentFactory.getInstance();
    }

    public void startActivity(Class T) {
        startActivity(T, null);
    }

    public void startActivity(Class T, Bundle bundle) {
        Intent intent = new Intent(getApplicationContext(), T);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        startActivity(intent);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    private void inflateToolbarLayout() {
        if (tb != null) {
            tb.setTitle(getToolbarTitle());
        }
        setSupportActionBar(tb);
        if (isUseLeftMenu()) {
            setDrawerLayout();
            tb.setNavigationIcon(R.drawable.ic_menu);
            tb.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    KeyboardUtils.hide(context);
                    dl.post(new Runnable() {
                        @Override
                        public void run() {
                            dl.openDrawer(GravityCompat.START);
                        }
                    });
                }
            });
        } else
            setBackEnabled(isBackEnable());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.homeIcon:
                gotoHomePage();
                break;
        }
        return true;
    }

    private void gotoHomePage() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void setDrawerLayout() {
        setDrawableToogle();
    }

    public void setDrawableToogle() {
        dt = new ActionBarDrawerToggle(this, dl, tb, R.string.app_name, R.string.app_name);
        dl.addDrawerListener(dt);
        dt.syncState();
    }

    protected void setBackEnabled(boolean backEnable) {
        if (backEnable) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            tb.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } else {
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    public void closeLeftMenu() {
        if (dl != null)
            dl.post(new Runnable() {
                @Override
                public void run() {
                    dl.closeDrawers();
                }
            });
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void lockScreen() {
        ProgressLoadingDialog progressLoadingDialog = new ProgressLoadingDialog();
        progressLoadingDialog.show(getSupportFragmentManager(), "progress");
    }

    @Override
    public void unlockScreen() {
        ProgressLoadingDialog progressLoadingDialog = (ProgressLoadingDialog) getSupportFragmentManager().findFragmentByTag("progress");
        if (progressLoadingDialog != null) {
            progressLoadingDialog.dismiss();
        }
    }

    public void setOnBackPressedListener(OnBackPressedListener listener) {
        onBackPressedListener = listener;
    }


    @Override
    public void onBackPressed() {
        if (isUseLeftMenu() && dl.isDrawerOpen(GravityCompat.START)) {
            dl.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void showLog(String message) {
        Log.d(getContext().getClass().getSimpleName(), message);
    }


}
