package com.project.t24.base;

import com.project.t24.data.T24Service;
import com.project.t24.util.RxBus;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mac on 27.09.2016.
 */

public class BaseInteractor {

    private T24Service t24Service;
    private RxBus rxBus;

    public BaseInteractor(T24Service t24Service, RxBus rxBus) {
        this.t24Service = t24Service;
        this.rxBus = rxBus;
    }

    public T24Service getT24Service() {

        return t24Service;
    }

    public RxBus getRxBus() {
        return rxBus;
    }

    public <T> Observable.Transformer<T, T> applySchedulers() {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> observable) {
                return observable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

}
