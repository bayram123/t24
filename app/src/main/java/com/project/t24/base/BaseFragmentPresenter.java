package com.project.t24.base;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.project.t24.util.RxBus;

import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by mac on 14.09.2016.
 */

public abstract class BaseFragmentPresenter<V extends BaseFragmentView> extends MvpBasePresenter<V> implements MvpPresenter<V> {

    protected CompositeSubscription cs;
    private RxBus rxBus;

    protected BaseFragmentPresenter(RxBus rxBus) {
        this.rxBus = rxBus;
    }

    public abstract void onResponse(Object response);

    @Override
    public void attachView(V view) {
        super.attachView(view);
        cs = new CompositeSubscription();
        cs.add(rxBus.toObserverable().subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().unlockScreen();
                    }

                    @Override
                    public void onNext(Object o) {
                        if (o instanceof ErrorEvent) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            getView().unlockScreen();
                            ErrorEvent errorEvent = (ErrorEvent) o;
                        } else {
                            onResponse(o);
                        }
                        getView().unlockScreen();

                    }
                })


        );

    }

    @Override
    public void detachView(boolean retainInstance) {
        cs.unsubscribe();
        super.detachView(retainInstance);
    }

    public RxBus getRxBus() {
        return rxBus;
    }


}
