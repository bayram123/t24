package com.project.t24.base;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.project.t24.interactors.NewsInteractor;
import com.project.t24.util.RxBus;
import com.project.t24.util.fragment.Page;

import javax.inject.Inject;

import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by mac on 3.08.2016.
 */

public abstract class BasePresenter<V extends BaseView> extends MvpBasePresenter<V> implements MvpPresenter<V> {

    protected CompositeSubscription cs;
    private RxBus rxBus;
    @Inject
    NewsInteractor newsInteractor;

    protected BasePresenter(RxBus rxBus) {
        this.rxBus = rxBus;
    }

    public abstract void onResponse(Object response);

    public void onLeftMenuItemSelected(int categoryId, String categoryName) {
        if (isViewAttached())
            getView().showPage(Page.CATEGORY, categoryId);
            getView().setToolbarTitle(categoryName);
    }

    @Override
    public void attachView(V view) {
        super.attachView(view);
        cs = new CompositeSubscription();
        cs.add(rxBus.toObserverable().subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().unlockScreen();
                    }

                    @Override
                    public void onNext(Object o) {
                        if (o instanceof ErrorEvent) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            getView().unlockScreen();
                            ErrorEvent errorEvent = (ErrorEvent) o;
                        } else {
                            onResponse(o);
                        }
                        getView().unlockScreen();

                    }
                })


        );

    }


    @Override
    public void detachView(boolean retainInstance) {
        cs.unsubscribe();
        super.detachView(retainInstance);
    }

    public RxBus getRxBus() {
        return rxBus;
    }
}
