package com.project.t24.data.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mac on 3.08.2016.
 */
public class News implements Parcelable {
    private String alias;
    private Category category;
    private String excerpt;
    private String id;
    private Images images;
    private String publishingDate;
    private Stats stats;
    private String title;
    private Urls urls;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public String getPublishingDate() {
        return publishingDate;
    }

    public void setPublishingDate(String publishingDate) {
        this.publishingDate = publishingDate;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public Urls getUrls() {
        return urls;
    }

    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.alias);
        dest.writeParcelable(this.category, flags);
        dest.writeString(this.excerpt);
        dest.writeString(this.id);
        dest.writeParcelable(this.images, flags);
        dest.writeString(this.publishingDate);
        dest.writeParcelable(this.stats, flags);
        dest.writeString(this.title);
        dest.writeParcelable(this.urls, flags);
    }

    public News() {
    }

    protected News(Parcel in) {
        this.alias = in.readString();
        this.category = in.readParcelable(Category.class.getClassLoader());
        this.excerpt = in.readString();
        this.id = in.readString();
        this.images = in.readParcelable(Images.class.getClassLoader());
        this.publishingDate = in.readString();
        this.stats = in.readParcelable(Stats.class.getClassLoader());
        this.title = in.readString();
        this.urls = in.readParcelable(Urls.class.getClassLoader());
    }

    public static final Parcelable.Creator<News> CREATOR = new Parcelable.Creator<News>() {
        @Override
        public News createFromParcel(Parcel source) {
            return new News(source);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };
}
