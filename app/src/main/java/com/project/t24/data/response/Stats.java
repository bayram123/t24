package com.project.t24.data.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zafer on 4.08.2016.
 */

public class Stats implements Parcelable {
    private int comments;
    private int interactions;
    private int likes;
    private int pageviews;
    private int reads;
    private int shares;

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public int getInteractions() {
        return interactions;
    }

    public void setInteractions(int interactions) {
        this.interactions = interactions;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getPageviews() {
        return pageviews;
    }

    public void setPageviews(int pageviews) {
        this.pageviews = pageviews;
    }

    public int getReads() {
        return reads;
    }

    public void setReads(int reads) {
        this.reads = reads;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.comments);
        dest.writeInt(this.interactions);
        dest.writeInt(this.likes);
        dest.writeInt(this.pageviews);
        dest.writeInt(this.reads);
        dest.writeInt(this.shares);
    }

    public Stats() {
    }

    protected Stats(Parcel in) {
        this.comments = in.readInt();
        this.interactions = in.readInt();
        this.likes = in.readInt();
        this.pageviews = in.readInt();
        this.reads = in.readInt();
        this.shares = in.readInt();
    }

    public static final Parcelable.Creator<Stats> CREATOR = new Parcelable.Creator<Stats>() {
        @Override
        public Stats createFromParcel(Parcel source) {
            return new Stats(source);
        }

        @Override
        public Stats[] newArray(int size) {
            return new Stats[size];
        }
    };
}
