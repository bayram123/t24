package com.project.t24.data.response;

import java.util.ArrayList;

/**
 * Created by mac on 1.11.2016.
 */

public class CategoryModel {
    private ArrayList<Category> data;
    private boolean result;

    public ArrayList<Category> getData() {
        return data;
    }

    public void setData(ArrayList<Category> data) {
        this.data = data;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
