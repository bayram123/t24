package com.project.t24.data;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.t24.BuildConfig;
import com.project.t24.util.RxBus;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * Created by mac on 1.10.2016.
 */

@Module(includes = {
        ApiModule.class
})

public class DataModule {

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Application app) {
        return app.getSharedPreferences("t24", Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    RxBus provideRxBus() {
        return new RxBus();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    Picasso providePicasso(Application app) {
        return new Picasso.Builder(app)
                .listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                        if (BuildConfig.DEBUG) {
                            exception.printStackTrace();
                        }
                    }
                })
                .build();
    }
    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Application app){
        return createOkHttpClient().build();
    }

    static OkHttpClient.Builder createOkHttpClient() {
        return new OkHttpClient.Builder();
    }


}
