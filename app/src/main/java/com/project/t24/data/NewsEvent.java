package com.project.t24.data;

import com.project.t24.data.response.NewsModel;

/**
 * Created by mac on 17.10.2016.
 */

public class NewsEvent {
    public NewsModel newsModel;
    public int newsOrder;

    public NewsEvent(NewsModel newsModel, int newsOrder) {
        this.newsOrder = newsOrder;
        this.newsModel = newsModel;
    }

    public NewsEvent(NewsModel newsModel) {
        this.newsModel = newsModel;
    }
}
