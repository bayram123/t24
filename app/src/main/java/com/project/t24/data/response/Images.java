package com.project.t24.data.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zafer on 4.08.2016.
 */

public class Images implements Parcelable {
    private String box;
    private String grid;
    private String list;
    private String page;

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.box);
        dest.writeString(this.grid);
        dest.writeString(this.list);
        dest.writeString(this.page);
    }

    public Images() {
    }

    protected Images(Parcel in) {
        this.box = in.readString();
        this.grid = in.readString();
        this.list = in.readString();
        this.page = in.readString();
    }

    public static final Parcelable.Creator<Images> CREATOR = new Parcelable.Creator<Images>() {
        @Override
        public Images createFromParcel(Parcel source) {
            return new Images(source);
        }

        @Override
        public Images[] newArray(int size) {
            return new Images[size];
        }
    };
}
