package com.project.t24.data;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mac on 1.10.2016.
 */
@Module

public class ApiModule {
    public static final HttpUrl BASE_URL = HttpUrl.parse("http://t24.com.tr/api/v3/");

    @Provides
    @Singleton
    HttpUrl provideBaseUrl() {
        return BASE_URL;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(HttpUrl baseUrl, OkHttpClient client, Gson gson) {
        return new Retrofit.Builder()
                .client(client).baseUrl(baseUrl)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

    }

    @Provides
    @Singleton
    T24Service provideT24Service(Retrofit retrofit){
        return retrofit.create(T24Service.class);

    }
}
