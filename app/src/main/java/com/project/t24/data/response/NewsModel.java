package com.project.t24.data.response;

import java.util.ArrayList;

/**
 * Created by mac on 3.08.2016.
 */

public class NewsModel {
    private ArrayList<News> data;
    private Paging paging;
    private boolean result;

    public ArrayList<News> getData() {
        return data;
    }

    public void setData(ArrayList<News> data) {
        this.data = data;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
