package com.project.t24.data.response;

/**
 * Created by mac on 18.08.2016.
 */

public class NewsDetail{
    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    private Data data;
    private boolean result;

}
