package com.project.t24.data;

import com.project.t24.data.response.CategoryModel;
import com.project.t24.data.response.NewsDetail;
import com.project.t24.data.response.NewsModel;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by mac on 2.08.2016.
 */

public interface T24Service {

    //http://t24.com.tr/api/v3/stories.json?paging=1      //10 tane haber
    @GET("stories.json")
    Observable<NewsModel> newsByPageNumber(@Query("paging") int paging);


    //http://t24.com.tr/api/v3/stories.json?story=355712  //Haber detayı.
    @GET("stories.json")
    Observable<NewsDetail> newsDetailById(@Query("story") int storyId);


    //http://t24.com.tr/api/v3/categories.json?type=story //Sol menüdeki kategoriler.
    @GET("categories.json?type=story")
    Observable<CategoryModel> newsCategory();


    //http://t24.com.tr/api/v3/stories.json?category=19  //Sol menüden seçilen kategorilere ait 10 haber.
    @GET("stories.json")
    Observable<NewsModel> newsByCategoryId(@Query("category") int categoryId);

}


