package com.project.t24.data.response;

/**
 * Created by mac on 18.08.2016.
 */
public class AuthorImages {
    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    private String page;
    private String grid;
    private String list;
}
