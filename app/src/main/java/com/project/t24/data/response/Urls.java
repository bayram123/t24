package com.project.t24.data.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zafer on 4.08.2016.
 */

public class Urls implements Parcelable {
    private String web;

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.web);
    }

    public Urls() {
    }

    protected Urls(Parcel in) {
        this.web = in.readString();
    }

    public static final Parcelable.Creator<Urls> CREATOR = new Parcelable.Creator<Urls>() {
        @Override
        public Urls createFromParcel(Parcel source) {
            return new Urls(source);
        }

        @Override
        public Urls[] newArray(int size) {
            return new Urls[size];
        }
    };
}
