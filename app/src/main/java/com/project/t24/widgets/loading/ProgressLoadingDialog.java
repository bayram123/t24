package com.project.t24.widgets.loading;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.t24.R;

/**
 * Created by zafer on 4.08.2016.
 */

public class ProgressLoadingDialog extends DialogFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.CustomDialogFragment);
    }

    // @NonNull
    // @Override
    // public Dialog onCreateDialog(Bundle savedInstanceState) {
//
    //     AlertDialog.Builder builder = new_order AlertDialog.Builder(getActivity());
    //     LayoutInflater inflater = getActivity().getLayoutInflater();
    //     builder.setView(inflater.inflate(R.layout.alert_dialog_progress, null));
//
//
    //     return builder.create();
    // }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.alert_dialog_progress, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }




}