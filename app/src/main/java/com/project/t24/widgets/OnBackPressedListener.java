package com.project.t24.widgets;

/**
 * Created by mac on 13.09.2016.
 */

public interface OnBackPressedListener {
    public void onBackPressed();

}
