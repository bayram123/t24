package com.project.t24;

import com.project.t24.data.DataModule;
import com.project.t24.interactors.InteractorsModule;
import com.project.t24.interactors.NewsInteractor;
import com.project.t24.util.RxBus;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by mac on 9.10.2016.
 */

@Singleton
@Component(
        modules = {
                AppModule.class,
                DataModule.class,
                InteractorsModule.class

        }
)


public interface AppComponent {

    void inject(App app);

    NewsInteractor getNewsInteractor();

    RxBus getRxBus();

    Picasso getPicasso();


}
