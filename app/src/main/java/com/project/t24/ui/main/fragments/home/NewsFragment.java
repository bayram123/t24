package com.project.t24.ui.main.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.t24.R;
import com.project.t24.data.response.News;
import com.project.t24.ui.detail.controller.DetailActivity;
import com.project.t24.util.NetworkUtils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.project.t24.util.NetworkUtils.getHtmlParsedSpanned;


public class NewsFragment extends Fragment {
    private Unbinder unbinder;

    @BindView(R.id.fragmentViewPager_news_image)
    ImageView ivNewsImage;
    @BindView(R.id.fragmentViewPager_news_tvTitle)
    TextView tvNewsTitle;
    private Spanned title;

    private News mNews;

    public NewsFragment() {
    }


    public static NewsFragment newInstance(News news) {
        NewsFragment fragment = new NewsFragment();
        if (news != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("news", news);
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNews = getArguments().getParcelable("news");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_pager, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNewsTitle.setText(getHtmlParsedSpanned(mNews.getTitle()));
        Picasso.with(getContext()).load(NetworkUtils.fixImageUrl(mNews.getImages().getPage())).into(ivNewsImage);
        ivNewsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                String newsId = mNews.getId();
                bundle.putString("newsId", newsId);
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}

