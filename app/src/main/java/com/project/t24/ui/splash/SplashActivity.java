package com.project.t24.ui.splash;

import android.support.annotation.NonNull;
import android.view.View;

import com.project.t24.AppComponent;
import com.project.t24.R;
import com.project.t24.base.BaseActivity;

public class SplashActivity extends BaseActivity<SplashView, SplashPresenter> implements SplashView {

    private SplashComponent component;

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public void onActivityStarted() {
        getPresenter().onActivityStarted();
    }

    @Override
    public void injectDependencies(AppComponent appComponent) {
        component = DaggerSplashComponent.builder().appComponent(appComponent).build();
        component.inject(this);
    }

    @NonNull
    @Override
    public SplashPresenter createPresenter() {
        return component.getSplashPresenter();
    }

    @Override
    public boolean isUseLeftMenu() {
        return false;
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
