package com.project.t24.ui.detail.fragments;

import com.project.t24.AppComponent;
import com.project.t24.base.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by mac on 15.10.2016.
 */

@FragmentScope
@Component(
        dependencies = AppComponent.class
)
public interface DetailFraComponent {
    void  inject(DetailFra detailFragment);
    DetailFraPresenter getDetailFragmentPresenter();
}
