package com.project.t24.ui.main.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.project.t24.data.response.News;
import com.project.t24.ui.main.fragments.home.NewsFragment;

import java.util.List;

/**
 * Created by mac on 4.08.2016.
 */

public class NewsPagerAdapter extends FragmentStatePagerAdapter {

    private List<News> mNewsList;

    public NewsPagerAdapter(FragmentManager fm, List<News> newsList) {
        super(fm);
        mNewsList = newsList;
    }

    @Override
    public Fragment getItem(int position) {
        return NewsFragment.newInstance(mNewsList.get(position));
    }

    @Override
    public int getCount() {
        return mNewsList.size();
    }
}
