package com.project.t24.ui.main.fragments.category;

import android.os.Bundle;

import com.project.t24.base.BaseFragmentPresenter;
import com.project.t24.data.response.NewsModel;
import com.project.t24.interactors.NewsInteractor;
import com.project.t24.ui.detail.controller.DetailActivity;
import com.project.t24.util.RxBus;

import javax.inject.Inject;

/**
 * Created by mac on 22.09.2016.
 */

public class CategoryFraPresenter extends BaseFragmentPresenter<CategoryFraView> {
    private NewsInteractor newsInteractor;

    @Inject
    protected CategoryFraPresenter(RxBus rxBus, NewsInteractor newsInteractor) {
        super(rxBus);
        this.newsInteractor = newsInteractor;

    }

    public void onFragmentStarted(int categoryId) {
        newsInteractor.newsByCategoryId(categoryId);
        getView().closeLeftMenu();
    }

    public void onItemClickeded(Bundle bundle) {
      getView().startActivity(DetailActivity.class,bundle);
    }

    @Override
    public void onResponse(Object response) {
        if (response instanceof NewsModel) {
            NewsModel newsModel = (NewsModel) response;
            if (getView() != null) {
                getView().setRvAdapter(newsModel.getData());
            }
        }
    }
}
