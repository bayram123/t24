package com.project.t24.ui.detail.controller;

import android.support.annotation.NonNull;
import android.view.View;

import com.project.t24.AppComponent;
import com.project.t24.base.BaseActivity;

public class DetailActivity extends BaseActivity<DetailView, DetailPresenter> implements DetailView {
    private DetailComponent component;

    @NonNull
    @Override
    public DetailPresenter createPresenter() {
        return component.getDetailPresenter();
    }


    @Override
    public String getToolbarTitle() {
        return "DETAY";
    }

    @Override
    public void onActivityStarted() {
        getPresenter().onActivityStarted();
    }

    @Override
    public void injectDependencies(AppComponent appComponent) {

        component = DaggerDetailComponent.builder().appComponent(appComponent).build();
        component.inject(this);

    }

    @Override
    public boolean isBackEnable() {
        return true;
    }

    @Override
    public boolean isUseLeftMenu() {
        return false;
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
