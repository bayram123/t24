package com.project.t24.ui.detail.controller;

import com.project.t24.base.BasePresenter;
import com.project.t24.util.RxBus;
import com.project.t24.util.fragment.Page;

import javax.inject.Inject;

/**
 * Created by mac on 18.08.2016.
 */

public class DetailPresenter extends BasePresenter<DetailView> {

    @Inject
    protected DetailPresenter(RxBus rxBus) {
        super(rxBus);
    }

    public void onActivityStarted() {
        if(isViewAttached()){
            getView().showPage(Page.DETAIL);
        }
    }

    @Override
    public void onResponse(Object response) {

    }
}
