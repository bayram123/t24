package com.project.t24.ui.main.fragments.home;

import com.project.t24.base.BaseFragmentView;
import com.project.t24.data.response.News;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mac on 11.09.2016.
 */

public interface HomeFragmentView extends BaseFragmentView {

    void setAdapter(ArrayList<News> newsList);

    void setViewPager(List<News> newsList);

    void updateNewsList(List<News> newsList);

    void notifyAdapter();

}
