package com.project.t24.ui.main.controller;

import android.support.annotation.NonNull;
import android.view.View;

import com.project.t24.AppComponent;
import com.project.t24.base.BaseActivity;

public class MainActivity extends BaseActivity<MainView, MainPresenter> implements MainView {

    private MainComponent component;

    @Override
    public void onActivityStarted() {
        setBackEnabled(false);
        getPresenter().onActivityStarted();
    }

    @Override
    public void injectDependencies(AppComponent appComponent) {
        component = DaggerMainComponent.builder().appComponent(appComponent).build();
        component.inject(this);
    }

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return component.getMainPresenter();
    }

    @Override
    public String getToolbarTitle() {
        return "ANA EKRAN";
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }



    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
