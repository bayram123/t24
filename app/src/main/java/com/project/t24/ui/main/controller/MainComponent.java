package com.project.t24.ui.main.controller;

import com.project.t24.AppComponent;
import com.project.t24.base.scopes.ActivityScope;

import dagger.Component;

/**
 * Created by mac on 9.10.2016.
 */

@ActivityScope
@Component(
        dependencies = AppComponent.class
)
public interface MainComponent {
    void inject(MainActivity mainActivity);
    MainPresenter getMainPresenter();
}

