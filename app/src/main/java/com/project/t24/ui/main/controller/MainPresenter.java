package com.project.t24.ui.main.controller;

import com.project.t24.base.BasePresenter;
import com.project.t24.data.response.CategoryModel;
import com.project.t24.interactors.NewsInteractor;
import com.project.t24.util.RxBus;
import com.project.t24.util.fragment.Page;

import javax.inject.Inject;

/**
 * Created by mac on 2.08.2016.
 */

public class MainPresenter extends BasePresenter<MainView> {

    NewsInteractor newsInteractor;

    @Inject
    public MainPresenter(RxBus rxBus, NewsInteractor newsInteractor) {
        super(rxBus);
        this.newsInteractor = newsInteractor;
    }


    public void onActivityStarted() {
        if (isViewAttached()) {
            getView().showPage(Page.HOME);
            leftMenuItems();
        }
    }

    public void leftMenuItems() {
        newsInteractor.newsCategory();
    }

    @Override
    public void onResponse(Object response) {
        if (response instanceof CategoryModel) {
            CategoryModel categoryModel = (CategoryModel) response;
            getView().setLeftMenuItems(categoryModel.getData());
        }
    }


}