package com.project.t24.ui.main.fragments.category;

import com.project.t24.AppComponent;
import com.project.t24.base.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by mac on 9.10.2016.
 */


@FragmentScope
@Component(
        dependencies = AppComponent.class
)
public interface CategoryFraComponent {
    void inject(CategoryFragment categoryFragment);
    CategoryFraPresenter getCategoryFragmentPresenter();
}
