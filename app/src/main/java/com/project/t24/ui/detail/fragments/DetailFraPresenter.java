package com.project.t24.ui.detail.fragments;

import com.project.t24.base.BaseFragmentPresenter;
import com.project.t24.data.response.NewsDetail;
import com.project.t24.interactors.NewsInteractor;
import com.project.t24.util.RxBus;

import javax.inject.Inject;

/**
 * Created by mac on 14.09.2016.
 */

public class DetailFraPresenter extends BaseFragmentPresenter<DetailFraView> {


    private NewsInteractor newsInteractor;

    @Inject
    protected DetailFraPresenter(RxBus rxBus, NewsInteractor newsInteractor) {
        super(rxBus);
        this.newsInteractor = newsInteractor;
    }


    public void onFragmentStarted(String newsId) {
        newsInteractor.newsDetailByStoryId(Integer.valueOf(newsId));

    }

    @Override
    public void onResponse(Object response) {
        if (response instanceof  NewsDetail) {
            NewsDetail newsDetail = (NewsDetail) response;
            getView().unlockScreen();
            getView().setDetailInformation(newsDetail);
        }

    }
}
