package com.project.t24.ui.splash;

import com.project.t24.AppComponent;
import com.project.t24.base.scopes.ActivityScope;

import dagger.Component;

/**
 * Created by mac on 21.10.2016.
 */
@ActivityScope
@Component(
        dependencies = AppComponent.class
)
public interface SplashComponent {

    void inject(SplashActivity splashActivity);

    SplashPresenter getSplashPresenter();

}
