package com.project.t24.ui.main.fragments.category;

import com.project.t24.base.BaseFragmentView;
import com.project.t24.data.response.News;

import java.util.ArrayList;

/**
 * Created by mac on 22.09.2016.
 */

public interface CategoryFraView extends BaseFragmentView {

    void setRvAdapter(ArrayList<News> newsList);

}
