package com.project.t24.ui.detail.fragments;

import com.project.t24.base.BaseFragmentView;
import com.project.t24.data.response.NewsDetail;

/**
 * Created by mac on 14.09.2016.
 */

public interface DetailFraView extends BaseFragmentView {
    void setDetailInformation(NewsDetail newsDetail);
}
