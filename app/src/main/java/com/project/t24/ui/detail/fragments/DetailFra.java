package com.project.t24.ui.detail.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.t24.AppComponent;
import com.project.t24.R;
import com.project.t24.base.BaseFragment;
import com.project.t24.data.response.NewsDetail;
import com.project.t24.util.NetworkUtils;
import com.project.t24.util.fragment.Page;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

import static com.project.t24.util.NetworkUtils.getHtmlParsedSpanned;

/**
 * Created by mac on 14.09.2016.
 */

public class DetailFra extends BaseFragment<DetailFraView, DetailFraPresenter> implements DetailFraView {


    @BindView(R.id.fragmentNewsDetail_iv)
    ImageView ivNews;
    @BindView(R.id.fragmentNewsDetail_tvTitle)
    TextView tvTitle;
    @BindView(R.id.fragmentNewsDetail_tvDetail)
    TextView tvNewsDetail;

    private DetailFraComponent component;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_news_detail;
    }

    @Override
    public void onFragmentStarted() {
        Intent intent = getBaseActivity().getIntent();
        Bundle bundle = intent.getExtras();
        String newsId = bundle.getString("newsId");
        getPresenter().onFragmentStarted(newsId);
    }

    @Override
    public Page getPage() {
        return Page.DETAIL;
    }

    @Override
    public void injectDependencies(AppComponent appComponent) {
        component = DaggerDetailFraComponent.builder().appComponent(appComponent).build();
        component.inject(this);

    }

    @NonNull
    @Override
    public DetailFraPresenter createPresenter() {
        return component.getDetailFragmentPresenter();

    }


    public static DetailFra newInstance() {
        return new DetailFra();
    }


    @Override
    public void setDetailInformation(NewsDetail newsDetail) {
        tvTitle.setText(getHtmlParsedSpanned(newsDetail.getData().getTitle()));
        tvNewsDetail.setText(getHtmlParsedSpanned(newsDetail.getData().getText()));
        Picasso.with(getBaseActivity()).load(NetworkUtils.fixImageUrl(newsDetail.getData().getImages().getPage())).into(ivNews);
    }
}
