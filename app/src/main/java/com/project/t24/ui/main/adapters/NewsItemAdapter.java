package com.project.t24.ui.main.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.t24.R;
import com.project.t24.data.response.News;
import com.project.t24.util.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.project.t24.util.NetworkUtils.getHtmlParsedSpanned;

/**
 * Created by mac on 4.08.2016.
 */

public class NewsItemAdapter extends RecyclerView.Adapter<NewsItemAdapter.ViewHolder> {

    private List<News> newsList;
    private Context mContext;
    Spanned title;
    private MainListener mainListener;

    public interface MainListener {
        void onItemClicked(String newsId);
    }

    public NewsItemAdapter(List<News> newsList, Context context, MainListener listener) {
        this.newsList = newsList;
        mContext = context;
        mainListener = listener;

    }

    @Override
    public NewsItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsItemAdapter.ViewHolder holder, int position) {
        Picasso.with(mContext).load(NetworkUtils.fixImageUrl(newsList.get(position).getImages().getPage())).into(holder.ivNewsImage);
        holder.tvNewsTitle.setText(getHtmlParsedSpanned(newsList.get(position).getTitle()));
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemNews_iv)
        ImageView ivNewsImage;
        @BindView(R.id.itemNews_tvTitle)
        TextView tvNewsTitle;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.cv_item_news)
        public void onClick() {
            mainListener.onItemClicked(newsList.get(getAdapterPosition()).getId());
        }
    }
}