package com.project.t24.ui.main.fragments.home;

import android.os.Bundle;

import com.project.t24.base.BaseFragmentPresenter;
import com.project.t24.data.NewsEnum;
import com.project.t24.data.NewsEvent;
import com.project.t24.data.response.News;
import com.project.t24.interactors.NewsInteractor;
import com.project.t24.ui.detail.controller.DetailActivity;
import com.project.t24.util.RxBus;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by mac on 11.09.2016.
 */

public class HomeFraPresenter extends BaseFragmentPresenter<HomeFragmentView> {
    private NewsInteractor newsInteractor;

    @Inject
    public HomeFraPresenter(RxBus rxBus, NewsInteractor newsInteractor) {
        super(rxBus);
        this.newsInteractor = newsInteractor;
    }


    public void onFragmentStarted() {
        getView().lockScreen();
        firstTenNews();
        secondTenNews();
    }

    public void firstTenNews() {
        newsInteractor.newsByPageNumber(1);
    }

    public void secondTenNews() {
        newsInteractor.newsByPageNumber(2);
    }

    public void onItemClicked(Bundle bundle) {
        getView().startActivity(DetailActivity.class, bundle);
    }

    public void onScrolleed(int page) {
        getView().lockScreen();
        newsInteractor.newsByPageNumber(page);
    }


    @Override
    public void onResponse(Object response) {
        if (response instanceof NewsEvent) {
            NewsEvent newsEvent = (NewsEvent) response;
            ArrayList<News> data = newsEvent.newsModel.getData();
            switch (newsEvent.newsOrder) {
                case NewsEnum.FIRST_TEN_NEWS:
                    getView().setViewPager(data);
                    getView().unlockScreen();
                    break;
                case NewsEnum.SECOND_TEN_NEWS:
                    getView().setAdapter(data);
                    getView().unlockScreen();
                    break;
                default:
                    getView().updateNewsList(data);
                    getView().notifyAdapter();
                    getView().unlockScreen();
                    break;
            }

        }

    }
}
