package com.project.t24.ui.main.fragments.home;

import com.project.t24.AppComponent;
import com.project.t24.base.scopes.FragmentScope;

import dagger.Component;

/**
 * Created by mac on 11.10.2016.
 */

@FragmentScope
@Component(
        dependencies = AppComponent.class
)
public interface HomeFraComponent {
    void inject(HomeFragment homeFragment);
    void inject(NewsFragment newsFragment);
    HomeFraPresenter getHomeFragmentPresenter();
}
