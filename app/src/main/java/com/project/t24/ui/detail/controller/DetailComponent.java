package com.project.t24.ui.detail.controller;

import com.project.t24.AppComponent;
import com.project.t24.base.scopes.ActivityScope;

import dagger.Component;

/**
 * Created by mac on 15.10.2016.
 */

@ActivityScope
@Component(
        dependencies = AppComponent.class
)
public interface DetailComponent {

    void inject(DetailActivity detailActivity);

    DetailPresenter getDetailPresenter();

}
