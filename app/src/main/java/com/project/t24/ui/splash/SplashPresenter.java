package com.project.t24.ui.splash;

import android.os.Handler;

import com.project.t24.base.BasePresenter;
import com.project.t24.ui.main.controller.MainActivity;
import com.project.t24.util.RxBus;

import javax.inject.Inject;

/**
 * Created by zafer on 1.08.2016.
 */

public class SplashPresenter extends BasePresenter<SplashView> {

    final Handler handler = new Handler();

    @Inject
    protected SplashPresenter(RxBus rxBus) {
        super(rxBus);
    }

    public void onActivityStarted() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getView().startActivity(MainActivity.class);
                getView().finishActivity();
            }
        }, 1000);
    }

    @Override
    public void onResponse(Object response) {

    }
}
