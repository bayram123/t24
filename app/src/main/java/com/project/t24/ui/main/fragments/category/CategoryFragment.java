package com.project.t24.ui.main.fragments.category;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.project.t24.AppComponent;
import com.project.t24.R;
import com.project.t24.base.BaseFragment;
import com.project.t24.data.response.News;
import com.project.t24.ui.main.adapters.NewsItemAdapter;
import com.project.t24.util.fragment.Page;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by mac on 22.09.2016.
 */

public class CategoryFragment extends BaseFragment<CategoryFraView, CategoryFraPresenter> implements CategoryFraView, NewsItemAdapter.MainListener {
    @BindView(R.id.fragmentCategory_rv)
    RecyclerView rv;
    private CategoryFraComponent component;
    NewsItemAdapter newsItemAdapter;
    private ArrayList<News> mNewsList = new ArrayList<>();
    private LinearLayoutManager llManager;
    private static final String CATEGORY_ID = "CATEGORY_ID";

    @Override
    public int getLayoutId() {
        return R.layout.fragment_category;
    }

    @Override
    public void onFragmentStarted() {
        int categoryId = getArguments().getInt(CATEGORY_ID, -1);
        getPresenter().onFragmentStarted(categoryId);
    }

    @Override
    public Page getPage() {
        return Page.CATEGORY;
    }

    @Override
    public void injectDependencies(AppComponent appComponent) {
        component = DaggerCategoryFraComponent.builder().appComponent(appComponent).build();
        component.inject(this);
    }

    @NonNull
    @Override
    public CategoryFraPresenter createPresenter() {
        return component.getCategoryFragmentPresenter();
    }

    public static CategoryFragment newInstance(Object... obj) {
        CategoryFragment categoryFragment = new CategoryFragment();
        int categoryId = (int) obj[0];
        Bundle bundle = new Bundle();
        bundle.putInt(CATEGORY_ID, categoryId);
        categoryFragment.setArguments(bundle);
        return categoryFragment;

    }

    @Override
    public void setRvAdapter(ArrayList<News> newsList) {
        mNewsList.addAll(newsList);
        llManager = new LinearLayoutManager(getBaseActivity());
        rv.setHasFixedSize(true);
        rv.setLayoutManager(llManager);
        newsItemAdapter = new NewsItemAdapter(mNewsList, getBaseActivity(), this);
        rv.setAdapter(newsItemAdapter);

    }

    @Override
    public void onItemClicked(String newsId) {
        Bundle bundle = new Bundle();
        bundle.putString("newsId", newsId);
        getPresenter().onItemClickeded(bundle);
    }
}
