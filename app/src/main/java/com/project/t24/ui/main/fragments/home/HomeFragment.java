package com.project.t24.ui.main.fragments.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.project.t24.AppComponent;
import com.project.t24.R;
import com.project.t24.base.BaseFragment;
import com.project.t24.data.response.News;
import com.project.t24.ui.main.adapters.NewsItemAdapter;
import com.project.t24.ui.main.adapters.NewsPagerAdapter;
import com.project.t24.util.fragment.Page;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

/**
 * Created by mac on 11.09.2016.
 */

public class HomeFragment extends BaseFragment<HomeFragmentView, HomeFraPresenter> implements HomeFragmentView, NewsItemAdapter.MainListener {

    @BindView(R.id.fragmentHome_vp)
    ViewPager vp;
    @BindView(R.id.fragmentHome_rl)
    RelativeLayout rl;
    @BindView(R.id.fragmentHome_rvNews)
    RecyclerView rv;
    @BindView(R.id.fragmentHome_indicator)
    CirclePageIndicator indicator;
    int currentPage = 0;
    int page = 3;
    int NUM_PAGES = 10;
    private ArrayList<News> mNewsList = new ArrayList<>();
    private HomeFraComponent component;
    NewsPagerAdapter newsPagerAdapter;
    NewsItemAdapter newsItemAdapter;
    LinearLayoutManager llManager;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onFragmentStarted() {
        getPresenter().onFragmentStarted();
        setToolbarTitle("Ana Ekran");
    }


    @Override
    public Page getPage() {
        return Page.HOME;
    }

    @Override
    public void injectDependencies(AppComponent appComponent) {

        component = DaggerHomeFraComponent.builder().appComponent(appComponent).build();
        component.inject(this);
    }

    @NonNull
    @Override
    public HomeFraPresenter createPresenter() {
        return component.getHomeFragmentPresenter();
    }

    @Override
    public void setAdapter(ArrayList<News> newsList) {
        mNewsList.addAll(newsList);
        llManager = new LinearLayoutManager(getBaseActivity());
        rv.setHasFixedSize(true);
        rv.setLayoutManager(llManager);
        newsItemAdapter = new NewsItemAdapter(mNewsList, getBaseActivity(), this);
        rv.setAdapter(newsItemAdapter);

        rv.addOnScrollListener(new EndlessRecyclerViewScrollListener(llManager, page) {
            @Override
            public void onLoadMore(int current_page) {
                getPresenter().onScrolleed(current_page);
                page++;
            }

            @Override
            public void hideViewPager() {
                rl.setVisibility(View.GONE);
            }

            @Override
            public void showViewPager() {
                rl.setVisibility(View.VISIBLE);
            }
        });
    }


    @Override
    public void setViewPager(final List<News> newsList) {
        newsPagerAdapter = new NewsPagerAdapter(getBaseActivity().getSupportFragmentManager(), newsList);
        vp.setAdapter(newsPagerAdapter);
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        indicator.setViewPager(vp);
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (getBaseActivity() != null) {
                    getBaseActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (currentPage == NUM_PAGES) {
                                currentPage = 0;
                            }
                            if (vp != null)
                                vp.setCurrentItem(currentPage++, true);

                        }

                    });
                }


            }

        }, 500, 1500);
    }


    @Override
    public void updateNewsList(List<News> newsList) {
        mNewsList.addAll(newsList);
    }

    @Override
    public void notifyAdapter() {
        newsItemAdapter.notifyDataSetChanged();
        rv.addOnScrollListener(new EndlessRecyclerViewScrollListener(llManager, page) {
            @Override
            public void onLoadMore(int current_page) {
                getPresenter().onScrolleed(current_page);
                page++;
            }

            @Override
            public void hideViewPager() {
                rl.setVisibility(View.GONE);
            }

            @Override
            public void showViewPager() {
                rl.setVisibility(View.VISIBLE);
            }


        });
    }


    @Override
    public void onItemClicked(String newsId) {
        Bundle bundle = new Bundle();
        bundle.putString("newsId", newsId);
        getPresenter().onItemClicked(bundle);
    }

}
