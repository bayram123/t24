package com.project.t24;

import android.app.Application;
import android.content.Context;

/**
 * Created by mac on 2.08.2016.
 */

public class App extends Application {


    private static App app;
    private AppComponent appComponent;

    public static synchronized App getInstance() {
        return app;
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        setupGraph();
    }

    private void setupGraph() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        appComponent.inject(this);
    }

    public AppComponent component() {
        return appComponent;
    }
}
