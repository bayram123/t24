package com.project.t24.interactors;

import android.util.Log;

import com.project.t24.base.BaseInteractor;
import com.project.t24.data.NewsEnum;
import com.project.t24.data.NewsEvent;
import com.project.t24.data.T24Service;
import com.project.t24.data.response.CategoryModel;
import com.project.t24.data.response.NewsDetail;
import com.project.t24.data.response.NewsModel;
import com.project.t24.util.RxBus;

import rx.Subscriber;

/**
 * Created by mac on 2.08.2016.
 */

public class NewsInteractor extends BaseInteractor {


    public NewsInteractor(T24Service t24Service, RxBus rxBus) {
        super(t24Service, rxBus);
    }


    public void newsByPageNumber(final int pageNumber) {

        getT24Service().newsByPageNumber(pageNumber).compose(this.<NewsModel>applySchedulers()).subscribe(new Subscriber<NewsModel>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(NewsModel newsModel) {
                switch (pageNumber) {
                    case NewsEnum.FIRST_TEN_NEWS:
                        getRxBus().send(new NewsEvent(newsModel, NewsEnum.FIRST_TEN_NEWS));
                        break;
                    case NewsEnum.SECOND_TEN_NEWS:
                        getRxBus().send(new NewsEvent(newsModel, NewsEnum.SECOND_TEN_NEWS));
                        break;
                    default:
                        getRxBus().send(new NewsEvent(newsModel));
                }

            }


        });
    }

    public void newsDetailByStoryId(int storyId) {
        getT24Service().newsDetailById(storyId).compose(this.<NewsDetail>applySchedulers()).subscribe(new Subscriber<NewsDetail>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(NewsDetail newsDetail) {

                getRxBus().send(newsDetail);
            }
        });
    }


    public void newsCategory() {
        getT24Service().newsCategory().compose(this.<CategoryModel>applySchedulers()).subscribe(new Subscriber<CategoryModel>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.e("bayram", "hata var ulaaaan" + e);

            }

            @Override
            public void onNext(CategoryModel categoryModel) {
                getRxBus().send(categoryModel);

            }
        });
    }

    public void newsByCategoryId(int categoryId) {
        getT24Service().newsByCategoryId(categoryId).compose(this.<NewsModel>applySchedulers()).subscribe(new Subscriber<NewsModel>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(NewsModel newsModel) {
                getRxBus().send(newsModel);
            }
        });
    }


}