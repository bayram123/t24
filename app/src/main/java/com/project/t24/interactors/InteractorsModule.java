package com.project.t24.interactors;

import com.project.t24.data.T24Service;
import com.project.t24.util.RxBus;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mac on 9.10.2016.
 */

@Module
public class InteractorsModule {

    @Provides
    public NewsInteractor provideNewsInteractor(T24Service t24Service, RxBus rxBus){
        return new NewsInteractor(t24Service,rxBus);
    }

}
