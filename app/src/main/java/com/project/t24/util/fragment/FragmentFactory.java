package com.project.t24.util.fragment;

import com.project.t24.ui.detail.fragments.DetailFra;
import com.project.t24.ui.main.fragments.category.CategoryFragment;
import com.project.t24.ui.main.fragments.home.HomeFragment;

/**
 * Created by mac on 11.09.2016.
 */

public class FragmentFactory {

    private static FragmentFactory instance;

    private FragmentFactory() {
    }

    public static FragmentFactory getInstance() {
        if (instance == null)
            instance = new FragmentFactory();
        return instance;
    }

    public FragmentBuilder getFragment(boolean isTablet, Page page, Object... obj) {
        FragmentBuilder fragmentBuilder = null;

        switch (page) {
            case HOME:
                fragmentBuilder = new FragmentBuilder().setFragment(HomeFragment.newInstance()).setTransactionAnimation(TransactionAnimation.ENTER_FROM_LEFT);
                break;

            case DETAIL:
                fragmentBuilder = new FragmentBuilder().setFragment(DetailFra.newInstance()).setTransactionAnimation(TransactionAnimation.ENTER_FROM_RIGHT);
                break;

            case CATEGORY:
                fragmentBuilder = new FragmentBuilder().setFragment(CategoryFragment.newInstance(obj)).setTransactionAnimation(TransactionAnimation.ENTER_FROM_RIGHT);
                break;
        }
        return fragmentBuilder;
    }
}
