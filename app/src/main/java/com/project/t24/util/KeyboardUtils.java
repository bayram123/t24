package com.project.t24.util;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

/**
 * Klavye ile ilgili islemlerin bulundugu utility sinifi
 *
 * @author imeneksetmob
 */
public class KeyboardUtils {
    /**
     * Klavyeyi gosterir
     *
     * @param context
     */
    public static void show(Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        } catch (Exception e) {
        }
    }

    /**
     * Klavyeyi gizler.
     *
     * @param context
     */
    public static void hide(Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
        }
    }
}
