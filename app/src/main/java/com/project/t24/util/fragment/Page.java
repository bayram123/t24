package com.project.t24.util.fragment;

import java.io.Serializable;

/**
 * Created by mac on 11.09.2016.
 */

public enum Page implements Serializable {

    HOME(false),
    DETAIL(false),
    CATEGORY(false);

    private boolean effectTitle;

    Page(boolean _effectTitle) {
        effectTitle = _effectTitle;
    }

    public boolean hasEffectToTitle() {
        return effectTitle;
    }
}
