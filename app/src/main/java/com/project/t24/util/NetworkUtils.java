package com.project.t24.util;

import android.net.Uri;
import android.text.Html;
import android.text.Spanned;

/**
 * Created by mac on 8.08.2016.
 */

public class NetworkUtils {
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    public static String fixImageUrl(String url) {
        return "http://" + Uri.encode(url, ALLOWED_URI_CHARS).substring(2);
    }

    @SuppressWarnings("deprecation")
    public static Spanned getHtmlParsedSpanned(String data) {
        Spanned parsedData;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            parsedData = Html.fromHtml(data, Html.FROM_HTML_MODE_LEGACY);
            return parsedData;

        } else {
            parsedData = Html.fromHtml(data);
            return parsedData;
        }
    }
}
