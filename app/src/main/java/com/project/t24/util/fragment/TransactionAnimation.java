package com.project.t24.util.fragment;

/**
 * Created by mac on 11.09.2016.
 */

public enum TransactionAnimation {
    NO_ANIM,
    ENTER_FROM_LEFT,
    ENTER_FROM_RIGHT,
    ENTER_FROM_BOTTOM,
    ENTER_WITH_ALPHA,
    ENTER_FROM_RIGHT_STACK,
    ENTER_FROM_RIGHT_NO_ENTRANCE;
}
